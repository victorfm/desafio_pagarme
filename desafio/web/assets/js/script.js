const form = document.querySelector("form");
const urlPagamento = form.getAttribute("action");
const totalItens = document.querySelector(".totalItens");
const qtdItens = document.querySelector(".qtdItens");
const totalFrete = document.querySelector(".totalFrete");
const totalPedido = document.querySelector(".totalPedido");
const frete = document.querySelector(".frete");
const loader = document.querySelector(".loader");

let callback = function(response) {
    let json = JSON.parse(response);

    if(json.mensagem)
    {
        alert(json.mensagem);

        if(json.url)
        {
            location.href = json.url;
        }
    }

    if(json.quantidades)
    {
        let tudoEsgotado = true;

        json.quantidades.forEach(quantidade => {
            let select = document.querySelector("#qtdFantasia_" + quantidade.Id);
            let valor = quantidade.Quantidade;

            while(select.firstChild)
            {
                select.removeChild(select.firstChild);
            }

            if(valor <= 0)
            {
                select.setAttribute("disabled", "");
                select.parentNode.querySelector(".esgotado").classList.remove("hidden");
                
                let option = new Option(0, 0);
                select.appendChild(option);
            }
            else
            {
                select.removeAttribute("disabled");
                select.parentNode.querySelector(".esgotado").classList.add("hidden");

                for(let i = 0; i <= valor; i++)
                {
                    let option = new Option(i, i);
                    select.appendChild(option);
                }

                tudoEsgotado = false;
            }

            let btn = document.querySelector("button[data-target='checkout']");

            if(tudoEsgotado)
            {
                btn.setAttribute("disabled", "");
            }
            else
            {
                btn.removeAttribute("disabled");
            }
        });
    }
}

let carregarTotalizador = function()
{
    let valorFrete = parseFloat(frete.innerHTML);

    let finalItens = 0;
    let finalQtd = 0;
    let finalFrete = 0;

    let fantasias = document.querySelectorAll(".fantasia").forEach(element => {
        let valor = parseFloat(element.querySelector(".somar").innerHTML);
        let qtd = element.querySelector("select").selectedIndex;

        finalQtd += qtd;
        finalItens += (valor * qtd);
        finalFrete += (valorFrete * qtd);
    });

    totalItens.innerHTML = finalItens.formatMoney(2);
    qtdItens.innerHTML = finalQtd;
    totalFrete.innerHTML = finalFrete.formatMoney(2);
    totalPedido.innerHTML = ((finalItens + finalFrete).formatMoney(2)); //.replace(",", ".").replace(".", ",");
}

let finalizarPedido = function(e)
{
    e.preventDefault();
    let btn = this;
    
    loader.classList.remove("hidden");
    btn.classList.add("hidden");

    let request = new XMLHttpRequest();
    let formData = new FormData(form);

    request.onreadystatechange = function() {
        if(request.readyState === 4)
        {
            callback(request.response);
            loader.classList.add("hidden");
            btn.classList.remove("hidden");
        }
    }

    request.open("POST", urlPagamento);
    request.send(formData);
}

let trocarAba = function() {
    const target = this.getAttribute("data-target");
    let trocar = true;

    if(target == "checkout")
    {
        let nenhumProduto = true;

        let fantasias = document.querySelectorAll(".fantasia");
        fantasias.forEach(element => {
            let select = element.querySelector("select");

            if(select.selectedIndex != 0)
            {
                nenhumProduto = false;
            }
        });

        trocar = !nenhumProduto;
    }

    if(trocar)
    {
        Array.prototype.forEach.call(document.querySelectorAll(".aba"), function (aba) {
            aba.classList.remove("active");
        });
    
        document.querySelector("." + target).classList.add("active");
    }
    else
    {
        alert("Nenhum produto selecionado");
    }
}

let verificarQuantidadeDisponivel = function()
{
    let request = new XMLHttpRequest();
    let formData = new FormData(form);

    request.onreadystatechange = function() {
        if(request.readyState === 4)
        {
            callback(request.response);
            carregarTotalizador();
        }
    }

    request.open("POST", "/quantidadeDisponivel");
    request.send(formData);
}

verificarQuantidadeDisponivel();

document.querySelectorAll(".trocarAba").forEach(element => {
    element.addEventListener("click", trocarAba);
});

document.querySelectorAll(".fantasia select").forEach(element => {
    element.addEventListener("change", carregarTotalizador);
});

document.querySelector("#dataLocacao").addEventListener("change", verificarQuantidadeDisponivel);

document.querySelector(".request").addEventListener("click", finalizarPedido);

VMasker(document.querySelector(".data")).maskPattern("99/99/9999");
VMasker(document.querySelector(".celular")).maskPattern("(99) 99999-9999");
VMasker(document.querySelector(".cep")).maskPattern("99999-999");
VMasker(document.querySelector(".cpf")).maskPattern("999.999.999-99");
VMasker(document.querySelector(".expiracao")).maskPattern("99/9999");
VMasker(document.querySelector(".numeros")).maskPattern("999999999");
VMasker(document.querySelector(".numeroCartao")).maskPattern("9999 9999 9999 9999");

Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "," : d, 
    t = t == undefined ? "." : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};