<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\FornecedorItem;
use AppBundle\Entity\ValorFrete;
use AppBundle\Entity\Venda;
use AppBundle\Entity\VendaItem;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repFornecedorItem = $manager->getRepository(FornecedorItem::class);
        $repValorFrete = $manager->getRepository(ValorFrete::class);
        $data = array();

        $fantasias = $repFornecedorItem->findBy(array("Ativo" => true));

        $datas = array();
        for($i = 0; $i < 10; $i++)
        {
            $dataRef = new \DateTime("now");
            $datas[] = $dataRef->modify("+$i day")->format("d/m/Y");
        }

        $frete = $repValorFrete->findOneBy(array("Ativo" => true));

        $data["itens"] = $fantasias;
        $data["datas"] = $datas;
        $data["frete"] = $frete;

        return $this->render('pagarme/index.html.twig', $data);
    }

    /**
     * @Route("/vendas", name="vendas")
     */
    public function vendasAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repVenda = $manager->getRepository(Venda::class);
        $data = array();

        $data["vendas"] = $repVenda->findBy([], ["Id" => "DESC"]);

        return $this->render('pagarme/vendas.html.twig', $data);
    }

    /**
     * @Route("/quantidadeDisponivel", name="quantidadeDisponivel", methods="POST")
     */
    public function quantidadeDisponivelAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repFornecedorItem = $manager->getRepository(FornecedorItem::class);
        $repVendaItem = $manager->getRepository(VendaItem::class);
        $post = $request->request;
        $data = array();
        $data["quantidades"] = array();

        $dataLocacao = \DateTime::createFromFormat('d/m/Y', $post->get("dataLocacao"))->format("d/m/Y");

        if(!$dataLocacao)
        {
            $data["mensagem"] = "Erro ao obter a data selecionada";
        }
        else
        {
            $itens = $repFornecedorItem->findBy(array("Ativo" => true));

            foreach($itens as $key => $value) 
            {
                $quantidadeItem = $value->getQuantidade();

                foreach($value->getVendaItem() as $indexVendaItem => $vendaItem)
                {
                    if($vendaItem->getVenda()->getPago() && $dataLocacao === $vendaItem->getDataLocacao()->format("d/m/Y"))
                    {
                        $quantidadeItem -= $vendaItem->getQuantidade();
                    }
                }

                $data["quantidades"][] = array("Id" => $value->getId(), "Quantidade" => $quantidadeItem);
            }
        }

        return new JsonResponse($data);
    }
}
