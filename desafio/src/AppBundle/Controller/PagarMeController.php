<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Entity\FornecedorItem;
use AppBundle\Entity\FornecedorDivisao;
use AppBundle\Entity\Venda;
use AppBundle\Entity\VendaItem;
use AppBundle\Entity\VendaDivisaoItem;
use AppBundle\Entity\ValorFrete;
use Doctrine\Common\Collections\ArrayCollection;
use PagarMe\Sdk\PagarMe;
use PagarMe\Sdk\Customer\Address;
use PagarMe\Sdk\Customer\Phone;

class PagarMeController extends Controller
{
    /**
     * @Route("/finalizar", name="finalizarPedido", methods="POST")
     */
    public function finalizarPedidoAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repFornecedorItem = $manager->getRepository(FornecedorItem::class);
        $repFornecedorDivisao = $manager->getRepository(FornecedorDivisao::class);
        $repValorFrete = $manager->getRepository(ValorFrete::class);
        $post = $request->request;
        $data = array();
        $mensagem = "";

        $frete = $repValorFrete->findOneBy(array("Ativo" => true));
        $total = 0;
        $venda = new Venda();
        $itens = new ArrayCollection();
        $dataLocacao = $post->get("dataLocacao");
        $concluir = true;

        // try
        // {
            foreach($post->all() as $key => $value) 
            {
                if(strpos($key, "fantasia") === 0) {
                    $quantidade = (int)$post->get("qtdFantasia_" . $value);

                    if($quantidade > 0)
                    {
                        $fornecedorItem = $repFornecedorItem->find($value);

                        if($fornecedorItem == null)
                        {
                            $mensagem = "Fantasia não encontrada.";
                            $concluir = false;
                            break;
                        }

                        $quantidadeItem = $fornecedorItem->getQuantidade();

                        foreach($fornecedorItem->getVendaItem() as $chaveVendaItem => $vendaItem)
                        {
                            if($vendaItem->getVenda()->getPago() && $dataLocacao === $vendaItem->getDataLocacao()->format("d/m/Y"))
                            {
                                $quantidadeItem -= $vendaItem->getQuantidade();
                            }
                        }

                        if($quantidadeItem < $quantidade)
                        {
                            $mensagem = $fornecedorItem->getFantasia() . " esgotada(o).";
                            $concluir = false;
                            break;
                        }

                        $totalItem = $quantidade * $fornecedorItem->getValor();

                        $vendaItem = new VendaItem();
                        $vendaItem->setVenda($venda);
                        $vendaItem->setFornecedorItem($fornecedorItem);
                        $vendaItem->setValorFrete($frete);
                        $vendaItem->setValorItem($totalItem);
                        $vendaItem->setQuantidade($quantidade);
                        $vendaItem->setDataLocacao(\DateTime::createFromFormat('d/m/Y', $dataLocacao));

                        $divisoes = $repFornecedorDivisao->findBy(array(
                            "Ativo" => true,
                            "FornecedorOrigem" => $fornecedorItem->getFornecedor()->getId()
                        ));

                        foreach($divisoes as $chave => $valor)
                        {
                            $tipo = $valor->getTipo();
                            $valorDivisao = 0;

                            if($tipo === "%")
                            {
                                $porcentagem = $valor->getValor() / 100;
                                $valorDivisao = $totalItem * $porcentagem;
                            }
                            else if ($tipo === "Frete")
                            {
                                $valorDivisao = $frete->getValor() * $quantidade;
                            }

                            $vendaDivisaoItem = new VendaDivisaoItem();
                            $vendaDivisaoItem->setFornecedorDivisao($valor);
                            $vendaDivisaoItem->setVendaItem($vendaItem);
                            $vendaDivisaoItem->setValor($valorDivisao);

                            $vendaItem->getVendaDivisaoItem()->add($vendaDivisaoItem);
                        }

                        $total += $totalItem + ($frete->getValor() * $quantidade);
                        $venda->getVendaItem()->add($vendaItem);
                    }
                }
            }

            if($concluir)
            {
                if($total > 0) 
                {
                    $campos = array();
                    $campos["nome"] = $post->get("nome");
                    $campos["email"] = $post->get("email");
                    $campos["cpf"] = $post->get("cpf");
                    $campos["dataNascimento"] = str_replace("/", "", $post->get("dataNascimento"));
                    $campos["sexo"] = $post->get("sexo");
                    $campos["celular"] = str_replace(["(", ")", "-"], ["", "", ""], $post->get("celular"));

                    $campos["endereco"] = $post->get("endereco");
                    $campos["numero"] = $post->get("numero");
                    $campos["bairro"] = $post->get("bairro");
                    $campos["cep"] = str_replace("-", "", $post->get("cep"));
                    $campos["estado"] = $post->get("estado");
                    $campos["cidade"] = $post->get("cidade");

                    $campos["nomeCartao"] = $post->get("nomeCartao");
                    $campos["numeroCartao"] = str_replace(" ", "", $post->get("numeroCartao"));
                    $campos["expiracaoCartao"] = str_replace("/", "", $post->get("expiracaoCartao"));

                    foreach($campos as $key => $value)
                    {
                        if(empty($value))
                        {
                            $mensagem = "Todos os campos precisam estar preenchidos.";
                            $concluir = false;
                            break;
                        }
                    }

                    if($concluir)
                    {
                        $expiracao = explode("/", $post->get("expiracaoCartao"));

                        $ano = (int)date("Y");
                        $mes = (int)date("m");

                        if((int)$expiracao[0] < $mes && (int)$expiracao[1] <= $ano)
                        {
                            $mensagem = "Validade do cartão passa da data atual.";
                            $concluir = false;
                        }

                        if($concluir)
                        {
                            $venda->setValorTotal($total);
                            $venda->setDataCadastro(new \DateTime("now"));
                            $venda->setPago(false);

                            $manager->persist($venda);
                            $manager->flush();

                            $apiKey = 'ak_test_EoQ0pv7EX8lczNxNANga3Uooj6pDMK';
                            $pagarMe = new PagarMe($apiKey);

                            $expiracaoFormatado = $expiracao[0] . substr($expiracao[1], 0, 2);

                            $card = $pagarMe->card()->create(
                                $campos["numeroCartao"],
                                $campos["nomeCartao"],
                                $expiracaoFormatado
                            );

                            $celular = explode(" ", $campos["celular"]);

                            $address = new Address(array(
                                "street" => $campos["endereco"],
                                "streetNumber" => $campos["numero"],
                                "neighborhood" => $campos["bairro"],
                                "zipcode" => $campos["cep"],
                                "country" => "br",
                                "state" => $campos["estado"],
                                "city" => $campos["cidade"]
                            ));

                            $phone = new Phone(array(
                                "ddd" => $celular[0],
                                "number" => $celular[1],
                                "ddi" => "55"
                            ));

                            $customer = $pagarMe->customer()->create(
                                $campos["nome"],
                                $campos["email"],
                                $campos["cpf"],
                                $address,
                                $phone,
                                $campos["dataNascimento"],
                                $campos["sexo"]
                            );

                            $transaction = $pagarMe->transaction()->creditCardTransaction(
                                ((int)$total * 100),
                                $card,
                                $customer,
                                1,
                                true,
                                $this->generateUrl("index", array(), UrlGeneratorInterface::ABSOLUTE_URL),
                                ["idPedido" => $venda->getId()]
                            );

                            sleep(3);

                            $transacao = $pagarMe->transaction()->get($transaction->getId());

                            if($transacao->getStatus() === "refused")
                            {
                                $mensagem = "Compra recusada, tente novamente mais tarde.";
                            }
                            else
                            {
                                $venda->setPago(true);
                                
                                $manager->persist($venda);
                                $manager->flush();

                                $mensagem = "Venda realizada com sucesso!";

                                $data["url"] = $this->generateUrl("vendas", array(), UrlGeneratorInterface::ABSOLUTE_URL);
                            }
                        }
                    }
                }
                else
                {
                    $mensagem = "Nenhum produto selecionado.";
                }
            }
        // }
        // catch (\Exception $e)
        // {
        //     $mensagem = "Erro ao realizar venda. Por favor, verifique seus dados e tente novamente.";
        // }

        $data["mensagem"] = $mensagem;

        return new JsonResponse($data);
    }
}