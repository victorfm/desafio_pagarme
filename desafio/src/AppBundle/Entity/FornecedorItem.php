<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fornecedor_item")
 */

class FornecedorItem {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="string", name="Fantasia", length=100)
     */
    private $Fantasia;

    /**
     * @ORM\Column(type="decimal", name="Valor", precision=10, scale=4)
     */
    private $Valor;

    /**
     * @ORM\Column(type="integer", name="Quantidade")
     */
    private $Quantidade;

    /**
     * @ORM\Column(type="boolean", name="Ativo")
     */
    private $Ativo;

    /**
     * @ORM\ManyToOne(targetEntity="Fornecedor", inversedBy="FornecedorItem")
     * @ORM\JoinColumn(name="IdFornecedor", referencedColumnName="Id", nullable=false)
     */
    private $Fornecedor;

    /**
     * @ORM\OneToMany(targetEntity="VendaItem", mappedBy="FornecedorItem", cascade={"persist"})
     */
    private $VendaItem;

    public function __construct() {
        $this->VendaItem = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getFantasia()
    {
        return $this->Fantasia;
    }

    /**
     * @return string
     */ 
    public function getValor()
    {
        return $this->Valor;
    }

    /**
     * @return int
     */ 
    public function getQuantidade()
    {
        return $this->Quantidade;
    }

    /**
     * @return boolean
     */ 
    public function getAtivo()
    {
        return $this->Ativo;
    }

    /**
     * @return Fornecedor
     */ 
    public function getFornecedor()
    {
        return $this->Fornecedor;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getVendaItem()
    {
        return $this->VendaItem;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setFantasia($Fantasia)
    {
        $this->Fantasia = $Fantasia;
    }

    public function setValor($Valor)
    {
        $this->Valor = $Valor;
    }

    public function setQuantidade($Quantidade)
    {
        $this->Quantidade = $Quantidade;
    }

    public function setAtivo($Ativo)
    {
        $this->Ativo = $Ativo;
    }

    public function setFornecedor($Fornecedor)
    {
        $this->Fornecedor = $Fornecedor;
    }

    public function setVendaItem($VendaItem)
    {
        $this->VendaItem = $VendaItem;
    }
}