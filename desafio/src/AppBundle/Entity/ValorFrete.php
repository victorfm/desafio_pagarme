<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="valor_frete")
 */

class ValorFrete {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="decimal", name="Valor", precision=10, scale=4)
     */
    private $Valor;

    /**
     * @ORM\Column(type="boolean", name="Ativo")
     */
    private $Ativo;

    /**
     * @ORM\OneToMany(targetEntity="VendaItem", mappedBy="ValorFrete", cascade={"persist"})
     */
    private $VendaItem;

    public function __construct() {
        $this->VendaItem = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getValor()
    {
        return $this->Valor;
    }

    /**
     * @return boolean
     */ 
    public function getAtivo()
    {
        return $this->Ativo;
    }

    /**
     * @return ValorFrete
     */ 
    public function getValorFrete()
    {
        return $this->ValorFrete;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getVendaItem()
    {
        return $this->VendaItem;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setValor($Valor)
    {
        $this->Valor = $Valor;
    }

    public function setAtivo($Ativo)
    {
        $this->Ativo = $Ativo;
    }

    public function setValorFrete($ValorFrete)
    {
        $this->ValorFrete = $ValorFrete;
    }

    public function setVendaItem($VendaItem)
    {
        $this->VendaItem = $VendaItem;
    }
}