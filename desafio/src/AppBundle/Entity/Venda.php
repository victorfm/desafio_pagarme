<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="venda")
 */

class Venda {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="decimal", name="ValorTotal", precision=10, scale=4)
     */
    private $ValorTotal;

    /**
     * @ORM\Column(type="datetime", name="DataCadastro")
     */
    private $DataCadastro;

    /**
     * @ORM\Column(type="boolean", name="Pago")
     */
    private $Pago;

    /**
     * @ORM\OneToMany(targetEntity="VendaItem", mappedBy="Venda", cascade={"persist"})
     */
    private $VendaItem;

    public function __construct() {
        $this->VendaItem = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getValorTotal()
    {
        return $this->ValorTotal;
    }

    /**
     * @return DateTime
     */ 
    public function getDataCadastro()
    {
        return $this->DataCadastro;
    }

    /**
     * @return boolean
     */ 
    public function getPago()
    {
        return $this->Pago;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getVendaItem()
    {
        return $this->VendaItem;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setValorTotal($ValorTotal)
    {
        $this->ValorTotal = $ValorTotal;
    }

    public function setDataCadastro($DataCadastro)
    {
        $this->DataCadastro = $DataCadastro;
    }

    public function setPago($Pago)
    {
        $this->Pago = $Pago;
    }

    public function setVendaItem($VendaItem)
    {
        $this->VendaItem = $VendaItem;
    }
}