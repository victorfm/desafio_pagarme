<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="venda_item")
 */

class VendaItem {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="decimal", name="ValorItem", precision=10, scale=4)
     */
    private $ValorItem;

    /**
     * @ORM\Column(type="integer", name="Quantidade")
     */
    private $Quantidade;

    /**
     * @ORM\Column(type="date", name="DataLocacao")
     */
    private $DataLocacao;

    /**
     * @ORM\ManyToOne(targetEntity="FornecedorItem", inversedBy="VendaItem")
     * @ORM\JoinColumn(name="IdFornecedorItem", referencedColumnName="Id", nullable=false)
     */
    private $FornecedorItem;

    /**
     * @ORM\ManyToOne(targetEntity="ValorFrete", inversedBy="VendaItem")
     * @ORM\JoinColumn(name="IdValorFrete", referencedColumnName="Id", nullable=false)
     */
    private $ValorFrete;

    /**
     * @ORM\OneToMany(targetEntity="VendaDivisaoItem", mappedBy="VendaItem", cascade={"persist"})
     */
    private $VendaDivisaoItem;

    /**
     * @ORM\ManyToOne(targetEntity="Venda", inversedBy="VendaItem")
     * @ORM\JoinColumn(name="IdVenda", referencedColumnName="Id", nullable=false)
     */
    private $Venda;

    public function __construct()
    {
        $this->VendaDivisaoItem = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getValorItem()
    {
        return $this->ValorItem;
    }

    /**
     * @return boolean
     */ 
    public function getQuantidade()
    {
        return $this->Quantidade;
    }

    /**
     * @return DateTime
     */ 
    public function getDataLocacao()
    {
        return $this->DataLocacao;
    }

    /**
     * @return FornecedorItem
     */ 
    public function getFornecedorItem()
    {
        return $this->FornecedorItem;
    }

    /**
     * @return ValorFrete
     */ 
    public function getValorFrete()
    {
        return $this->ValorFrete;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getVendaDivisaoItem()
    {
        return $this->VendaDivisaoItem;
    }

    /**
     * @return Venda
     */ 
    public function getVenda()
    {
        return $this->Venda;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setValorItem($ValorItem)
    {
        $this->ValorItem = $ValorItem;
    }

    public function setQuantidade($Quantidade)
    {
        $this->Quantidade = $Quantidade;
    }

    public function setDataLocacao($DataLocacao)
    {
        $this->DataLocacao = $DataLocacao;
    }

    public function setFornecedorItem($FornecedorItem)
    {
        $this->FornecedorItem = $FornecedorItem;
    }

    public function setValorFrete($ValorFrete)
    {
        $this->ValorFrete = $ValorFrete;
    }

    public function setVendaDivisaoItem($VendaDivisaoItem)
    {
        $this->VendaDivisaoItem = $VendaDivisaoItem;
    }

    public function setVenda($Venda)
    {
        $this->Venda = $Venda;
    }
}