<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fornecedor")
 */

class Fornecedor {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="string", name="Nome", length=100)
     */
    private $Nome;

    /**
     * @ORM\OneToMany(targetEntity="FornecedorDivisao", mappedBy="FornecedorOrigem", cascade={"persist"})
     */
    private $FornecedorDivisaoOrigem;

    /**
     * @ORM\OneToMany(targetEntity="FornecedorDivisao", mappedBy="FornecedorDestino", cascade={"persist"})
     */
    private $FornecedorDivisaoDestino;

    /**
     * @ORM\OneToMany(targetEntity="FornecedorItem", mappedBy="Fornecedor", cascade={"persist"})
     */
    private $FornecedorItem;

    public function __construct() 
    {
        $this->FornecedorItem = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getNome()
    {
        return $this->Nome;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getFornecedorItem()
    {
        return $this->FornecedorItem;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getFornecedorDivisaoOrigem()
    {
        return $this->FornecedorDivisaoOrigem;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getFornecedorDivisaoDestino()
    {
        return $this->FornecedorDivisaoDestino;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setNome($Nome)
    {
        $this->Nome = $Nome;
    }

    public function setFornecedorItem($FornecedorItem)
    {
        $this->FornecedorItem = $FornecedorItem;
    }

    public function setFornecedorDivisaoOrigem($FornecedorDivisaoOrigem)
    {
        $this->FornecedorDivisaoOrigem = $FornecedorDivisaoOrigem;
    }

    public function setFornecedorDivisaoDestino($FornecedorDivisaoDestino)
    {
        $this->FornecedorDivisaoDestino = $FornecedorDivisaoDestino;
    }
}