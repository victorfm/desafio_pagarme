<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fornecedor_divisao")
 */

class FornecedorDivisao {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="string", name="Tipo", length=10)
     */
    private $Tipo;

    /**
     * @ORM\Column(type="decimal", name="Valor", precision=10, scale=4)
     */
    private $Valor;

    /**
     * @ORM\Column(type="boolean", name="Ativo")
     */
    private $Ativo;

    /**
     * @ORM\ManyToOne(targetEntity="Fornecedor", inversedBy="FornecedorDivisaoOrigem")
     * @ORM\JoinColumn(name="IdFornecedorOrigem", referencedColumnName="Id", nullable=false)
     */
    private $FornecedorOrigem;

    /**
     * @ORM\ManyToOne(targetEntity="Fornecedor", inversedBy="FornecedorDivisaoDestino")
     * @ORM\JoinColumn(name="IdFornecedorDestino", referencedColumnName="Id", nullable=false)
     */
    private $FornecedorDestino;

    /**
     * @ORM\OneToMany(targetEntity="VendaDivisaoItem", mappedBy="FornecedorDivisao", cascade={"persist"})
     */
    private $VendaDivisaoItem;

    public function __construct() {
        $this->VendaDivisaoItem = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getTipo()
    {
        return $this->Tipo;
    }

    /**
     * @return string
     */ 
    public function getValor()
    {
        return $this->Valor;
    }

    /**
     * @return boolean
     */ 
    public function getAtivo()
    {
        return $this->Ativo;
    }

    /**
     * @return Fornecedor
     */ 
    public function getFornecedorOrigem()
    {
        return $this->FornecedorOrigem;
    }

    /**
     * @return Fornecedor
     */ 
    public function getFornecedorDestino()
    {
        return $this->FornecedorDestino;
    }

    /**
     * @return ArrayCollection
     */ 
    public function getVendaDivisaoItem()
    {
        return $this->VendaDivisaoItem;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setTipo($Tipo)
    {
        $this->Tipo = $Tipo;
    }

    public function setValor($Valor)
    {
        $this->Valor = $Valor;
    }

    public function setAtivo($Ativo)
    {
        $this->Ativo = $Ativo;
    }

    public function setFornecedorOrigem($FornecedorOrigem)
    {
        $this->FornecedorOrigem = $FornecedorOrigem;
    }

    public function setFornecedorDestino($FornecedorDestino)
    {
        $this->FornecedorDestino = $FornecedorDestino;
    }

    public function setVendaDivisaoItem($VendaDivisaoItem)
    {
        $this->VendaDivisaoItem = $VendaDivisaoItem;
    }
}