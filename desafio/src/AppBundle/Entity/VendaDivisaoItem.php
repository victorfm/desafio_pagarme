<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="venda_divisao_item")
 */

class VendaDivisaoItem {

    /**
     * @ORM\Column(type="integer", name="Id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $Id;

    /**
     * @ORM\Column(type="decimal", name="Valor", precision=10, scale=4)
     */
    private $Valor;

    /**
     * @ORM\ManyToOne(targetEntity="FornecedorDivisao", inversedBy="VendaDivisaoItem")
     * @ORM\JoinColumn(name="IdFornecedorDivisao", referencedColumnName="Id", nullable=false)
     */
    private $FornecedorDivisao;

    /**
     * @ORM\ManyToOne(targetEntity="VendaItem", inversedBy="VendaDivisaoItem")
     * @ORM\JoinColumn(name="IdVendaItem", referencedColumnName="Id", nullable=false)
     */
    private $VendaItem;

    public function __construct() 
    {
        $this->VendaItem = new ArrayCollection();
        $this->FornecedorDivisao = new ArrayCollection();
    }

    /**
     * @return int
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return string
     */ 
    public function getValor()
    {
        return $this->Valor;
    }

    /**
     * @return int
     */ 
    public function getQuantidade()
    {
        return $this->Quantidade;
    }

    /**
     * @return FornecedorDivisao
     */ 
    public function getFornecedorDivisao()
    {
        return $this->FornecedorDivisao;
    }

    /**
     * @return VendaItem
     */ 
    public function getVendaItem()
    {
        return $this->VendaItem;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setValor($Valor)
    {
        $this->Valor = $Valor;
    }

    public function setQuantidade($Quantidade)
    {
        $this->Quantidade = $Quantidade;
    }

    public function setFornecedorDivisao($FornecedorDivisao)
    {
        $this->FornecedorDivisao = $FornecedorDivisao;
    }

    public function setVendaItem($VendaItem)
    {
        $this->VendaItem = $VendaItem;
    }
}