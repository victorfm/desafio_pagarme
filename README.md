Esse é a minha interpretação e solução do desafio proposto pela empresa Pagar.me.

Abaixo, o enunciado do desafio:

**Desafio: Marketplace**

Maria Barros decidiu abrir sua startup de aluguel de fantasias de carnaval, onde outros fornecedores também poderão vender suas fantasias conforme combinado. As regras de venda são:

* Para cada fantasia alugada produzida pela Maria, o valor total da venda + R$ 42,00 fixo de frete pertencem à Maria.
* Para cada fantasia alugada de outros fornecedores, o valor total da venda deverá ser dividido da seguinte forma:
* 15% do valor do produto vai para a Maria.
* 85% + R$ 42,00 de frete para o parceiro.

Abaixo temos algumas das fantasias oferecidas na startup da Maria:

FANTASIA | FORNECEDOR | VALOR (unidade)
-------- | ---------- | ---------------
Fantasia do Darth Vader | Maria Barros | R$ 125,00
Fantasia do Cafú | João Thiago Samuel Cavalcanti | R$ 100,00
Máscara de Cavalo | César Anthony João Martins | R$ 150,00

Lembrando que ainda há o valor do frete de R$ 42,00, por compra, e de responsabilidade da Maria.

A sua missão é elaborar uma página de checkout que exiba os produtos acima e nela deve haver um botão para finalizar a compra. A finalização da compra deve ser feita utilizando o checkout Pagar.me e ela deve ser autorizada e capturada após isso, exibindo uma página de sucesso.
 
**Requisitos**

* Entender a melhor forma que o Pagar.me pode solucionar o problema apresentado, você pode consultar na nossa documentação;
* A compra deve ser realizada com cartão de crédito;
* O desafio é livre para fazer com qualquer recurso, seja PHP puro ou com uso de algum framework, utilizando a última versão do nosso SDK de PHP;
* Deve haver pelo menos 1 locação envolvendo todos os produtos do marketplace;
* Não é necessário persistir nenhum dado no lado do cliente.

* * *

# Desenvolvimento

Usei PHP com o Framework Symfony (3.4.18), Javascript, CSS e HTML.

Antes de rodar o projeto, é necessário rodar o dump.sql em um servidor mysql.

Talvez seja necessário alterar as credenciais do banco, no arquivo desafio/app/config/parameters.yml

Depois de executar o dump.sql, abra um terminal, entre na pasta "desafio" e execute os seguintes comandos:

 * composer install
 * php bin/console server:run
 
 e acessar a url "localhost:8000".

* * *

Para solucionar o problema proposto, elaborei a seguinte modelagem:

![Modelagem](https://i.imgur.com/NDDIXdF.png)

 * Fornecedor: Representa o fornecedor;
 * Fornecedor_Item: Itens do fornecedor, caso ele queria ter mais do que um produto só;
 * Fornecedor_Divisão: Representa como será feita a divisão dos valores dos itens dos fornecedores;
 * Venda: Representa uma venda em si;
 * Venda_Item: Representa os itens que foram pedidos em uma venda;
 * Venda_divisão_item: Representa a divisão dos valores e do frete de um item vendido;
 * Valor_frete: Controle de frete.
 
 **Observações:**
 
 * Por que é cobrado um frete para cada item?
 
 Esse é a principal "pedra" que eu encontrei enquanto solucionava esse problema. O enunciado fala que:
 
 * "Lembrando que ainda há o valor do frete de R$ 42,00, por compra, e de responsabilidade da Maria.", 
 * "Para cada fantasia alugada produzida pela Maria, o valor total da venda + R$ 42,00 fixo de frete pertencem à Maria" 
 * "Para cada fantasia alugada de outros fornecedores, o valor total da venda deverá ser dividido da seguinte forma ... 85% + R$ 42,00 de frete para o parceiro." 
 
 Eu não sabia se era cobrado somente um frete pela compra toda, independente da quantidade de itens, ou se era cobrado um frete para cada item, devido a divisão dos valores. Então decidi jogar um frete por item, pois era o que mais fazia sentido na minha cabeça.