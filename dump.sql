CREATE DATABASE pagarme;
use pagarme;

CREATE TABLE fornecedor (Id INT AUTO_INCREMENT NOT NULL, Nome VARCHAR(100) NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

CREATE TABLE fornecedor_divisao (Id INT AUTO_INCREMENT NOT NULL, Tipo VARCHAR(10) NOT NULL, Valor NUMERIC(10, 4) NOT NULL, Ativo TINYINT(1) NOT NULL, IdFornecedorOrigem INT NOT NULL, IdFornecedorDestino INT NOT NULL, INDEX IDX_C2A683B9D1AC04F2 (IdFornecedorOrigem), INDEX IDX_C2A683B9A72175C (IdFornecedorDestino), PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

CREATE TABLE fornecedor_item (Id INT AUTO_INCREMENT NOT NULL, Fantasia VARCHAR(100) NOT NULL, Valor NUMERIC(10, 4) NOT NULL, Quantidade INT NOT NULL, Ativo TINYINT(1) NOT NULL, IdFornecedor INT NOT NULL, INDEX IDX_909B028D8A73A6B2 (IdFornecedor), PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

CREATE TABLE valor_frete (Id INT AUTO_INCREMENT NOT NULL, Valor NUMERIC(10, 4) NOT NULL, Ativo TINYINT(1) NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

CREATE TABLE venda (Id INT AUTO_INCREMENT NOT NULL, ValorTotal NUMERIC(10, 4) NOT NULL, DataCadastro DATETIME NOT NULL, Pago TINYINT(1) NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

CREATE TABLE venda_divisao_item (Id INT AUTO_INCREMENT NOT NULL, Valor NUMERIC(10, 4) NOT NULL, IdFornecedorDivisao INT NOT NULL, IdVendaItem INT NOT NULL, INDEX IDX_B01778EA810AED7C (IdFornecedorDivisao), INDEX IDX_B01778EAF51ED59A (IdVendaItem), PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

CREATE TABLE venda_item (Id INT AUTO_INCREMENT NOT NULL, ValorItem NUMERIC(10, 4) NOT NULL, Quantidade INT NOT NULL, DataLocacao DATE NOT NULL, IdFornecedorItem INT NOT NULL, IdValorFrete INT NOT NULL, IdVenda INT NOT NULL, INDEX IDX_F305FDDADCD86DE8 (IdFornecedorItem), INDEX IDX_F305FDDA5B420D7E (IdValorFrete), INDEX IDX_F305FDDA1D5C8685 (IdVenda), PRIMARY KEY(Id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

ALTER TABLE fornecedor_divisao ADD CONSTRAINT FK_C2A683B9D1AC04F2 FOREIGN KEY (IdFornecedorOrigem) REFERENCES fornecedor (Id);
ALTER TABLE fornecedor_divisao ADD CONSTRAINT FK_C2A683B9A72175C FOREIGN KEY (IdFornecedorDestino) REFERENCES fornecedor (Id);
ALTER TABLE fornecedor_item ADD CONSTRAINT FK_909B028D8A73A6B2 FOREIGN KEY (IdFornecedor) REFERENCES fornecedor (Id);
ALTER TABLE venda_divisao_item ADD CONSTRAINT FK_B01778EA810AED7C FOREIGN KEY (IdFornecedorDivisao) REFERENCES fornecedor_divisao (Id);
ALTER TABLE venda_divisao_item ADD CONSTRAINT FK_B01778EAF51ED59A FOREIGN KEY (IdVendaItem) REFERENCES venda_item (Id);
ALTER TABLE venda_item ADD CONSTRAINT FK_F305FDDADCD86DE8 FOREIGN KEY (IdFornecedorItem) REFERENCES fornecedor_item (Id);
ALTER TABLE venda_item ADD CONSTRAINT FK_F305FDDA5B420D7E FOREIGN KEY (IdValorFrete) REFERENCES valor_frete (Id);
ALTER TABLE venda_item ADD CONSTRAINT FK_F305FDDA1D5C8685 FOREIGN KEY (IdVenda) REFERENCES venda (Id);

INSERT INTO valor_frete (Id, Valor, Ativo) VALUES (0, 42.00, 1);

INSERT INTO fornecedor (Id, Nome) VALUES (0, 'Maria Barros');
INSERT INTO fornecedor (Id, Nome) VALUES (0, 'João Thiago Samuel Cavalcanti');
INSERT INTO fornecedor (Id, Nome) VALUES (0, 'César Anthony João Martins');

INSERT INTO fornecedor_item (Id, IdFornecedor, Fantasia, Valor, Quantidade, Ativo) VALUES (0, 1, 'Fantasia do Darth Vader', 125.00, 1, 1);
INSERT INTO fornecedor_item (Id, IdFornecedor, Fantasia, Valor, Quantidade, Ativo) VALUES (0, 2, 'Fantasia do Cafú', 100.00, 1, 1);
INSERT INTO fornecedor_item (Id, IdFornecedor, Fantasia, Valor, Quantidade, Ativo) VALUES (0, 3, 'Máscara de Cavalo', 150.00, 5, 1);

INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 1, 1, '%', 100.00, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 1, 1, 'Frete', 0, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 2, 2, '%', 85.00, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 2, 2, 'Frete', 0, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 2, 1, '%', 15.00, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 3, 3, '%', 85.00, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 3, 3, 'Frete', 0, 1);
INSERT INTO fornecedor_divisao (Id, IdFornecedorOrigem, IdFornecedorDestino, Tipo, Valor, Ativo) VALUES (0, 3, 1, '%', 15.00, 1);